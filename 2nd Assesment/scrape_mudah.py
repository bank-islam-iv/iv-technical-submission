import requests
from bs4 import BeautifulSoup
import pandas as pd
import datetime
import pyarrow.parquet as pq

# Function to scrape Mudah property listings in Johor
def scrape_mudah_johor():
    base_url = 'https://www.mudah.my/malaysia/properties-for-sale'
    johor_url = f'{base_url}/johor'
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'}

    # Initialize lists to store data
    ads_name_list = []
    ads_datetime_list = []
    location_list = []
    price_list = []
    property_type_list = []
    sqft_list = []

    # Scrape multiple pages (adjust range for more pages)
    for page_num in range(1, 6):
        page_url = f'{johor_url}?o={page_num}'
        response = requests.get(page_url, headers=headers)
        soup = BeautifulSoup(response.content, 'html.parser')

        # Extract property listings
        listings = soup.find_all('div', class_='listing_params_container')

        for listing in listings:
            try:
                ads_name = listing.find('h2', class_='list_title').text.strip()
                ads_datetime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                location = listing.find('div', class_='area_title').text.strip()
                price = listing.find('div', class_='ads_price').text.strip()
                property_type = listing.find('div', class_='list_ads_highlight').text.strip()
                sqft = listing.find('div', class_='lst_sqft').text.strip()

                ads_name_list.append(ads_name)
                ads_datetime_list.append(ads_datetime)
                location_list.append(location)
                price_list.append(price)
                property_type_list.append(property_type)
                sqft_list.append(sqft)
            except Exception as e:
                print(f"Error processing listing: {e}")

    # Create DataFrame
    df = pd.DataFrame({
        'Ads Name': ads_name_list,
        'Ads Datetime': ads_datetime_list,
        'Location': location_list,
        'Price': price_list,
        'Property Type': property_type_list,
        'Sqft': sqft_list
    })

    return df

# Main function to execute scraping and save as Parquet file
def main():
    # Scrape Mudah Johor properties
    df = scrape_mudah_johor()

    # Save to Parquet file partitioned by Ads Date
    current_date = datetime.datetime.now().strftime('%Y-%m-%d')
    file_name = f'mudah_properties_johor_{current_date}.parquet'
    pq.write_to_dataset(df, root_path=file_name, partition_cols=['Ads Datetime'])
    print(f"Data saved as Parquet file: {file_name}")

if __name__ == "__main__":
    main()
