/*================================================================================ Student Table ================================================================================*/
CREATE TABLE STUDENT (
	Sid INT,
    Name VARCHAR(100),
    Birthdate DATE,
    Gender VARCHAR(50),
    PRIMARY KEY (sid, name)
);

INSERT INTO STUDENT (Sid, Name, Birthdate, Gender) VALUES
(6807, 'Colin Freeman', '1990-06-18', 'Male'),
(1315, 'James Maldonado', '1990-07-10', 'Male'),
(1394, 'Donald Dominguez', '1990-11-08', 'Male'),
(6588, 'Gary Ross', '1990-11-05', 'Male'),
(8699, 'Lynn Hensley', '1990-03-31', 'Male'),
(8326, 'Michael Norris', '1990-04-28', 'Female'),
(3720, 'Ashley Dunlap', '1990-07-19', 'Male'),
(5360, 'Chelsea Bautista', '1990-09-07', 'Male'),
(8642, 'Steven Joyce', '1990-09-23', 'Male'),
(6348, 'Deanna Baker', '1990-09-08', 'Female'),
(3960, 'Miguel Chen', '1990-04-02', 'Female'),
(2933, 'Alyssa Jimenez', '1990-04-08', 'Male'),
(2529, 'Aaron Lewis', '1990-09-12', 'Female'),
(2899, 'Heather Tate', '1990-11-01', 'Female'),
(3929, 'Jennifer Higgins', '1990-03-20', 'Male'),
(3038, 'Charles Perez', '1980-12-04', 'Male'),
(7567, 'Tiffany Mendoza', '1980-01-10', 'Male'),
(2498, 'Logan Crawford MD', '1980-04-13', 'Male'),
(2398, 'Christopher Ellis', '1980-09-28', 'Female'),
(6576, 'Annette Barrett', '1980-04-26', 'Male'),
(5816, 'Sheri Williams', '1995-11-23', 'Male'),
(6473, 'Bridget Levy', '1995-04-24', 'Male'),
(3282, 'Kevin Miller', '1995-02-06', 'Male'),
(8574, 'Christopher Fischer', '1995-08-12', 'Female'),
(2671, 'Dr. Kelly Faulkner', '1995-05-19', 'Female'),
(2345, 'Barry Allen', '1995-11-01', 'Male'),
(9872, 'Oliver Queen', '1995-11-02', 'Male'),
(9462, 'Caitlin Snow', '1995-11-03', 'Female'),
(5732, 'Felicity Smoak', '1995-11-04', 'Female');


/*================================================================================ Teacher Table ================================================================================*/
CREATE TABLE Teacher (
    Tid INT PRIMARY KEY,
    Name NVARCHAR(100),
    Create_dt DATETIME,
    Update_dt DATETIME
);

INSERT INTO Teacher (Tid, Name, Create_dt, Update_dt) VALUES
(1111, 'Emma', '2014-03-18 12:00:00.000', GETDATE()),
(2222, 'Bob', '2013-07-10 14:50:00.000', GETDATE()),
(3333, 'Charlie', '2012-11-08 16:08:00.000', GETDATE()),
(4444, 'Alice', '2015-01-05 22:14:00.000', GETDATE()),
(5555, 'Luffy', '2012-06-30 16:51:00.000', GETDATE()),
(6666, 'Sanji', '2014-04-28 13:36:00.000', GETDATE()),
(7777, 'Usop', '2013-07-19 19:42:00.000', GETDATE());
 

 /*================================================================================ Course Table ================================================================================*/
CREATE TABLE Course (
    cid INT PRIMARY KEY,
    Name NVARCHAR(100),
    Tid INT,
    Create_dt DATETIME,
    Update_dt DATETIME
);
 
 -- Insert data into Course table
INSERT INTO Course (cid, name, tid, create_dt, update_dt)
VALUES
    (11, 'Mandarin', 1111, '2010-04-15 12:00:00.000', GETDATE()),
    (22, 'Math', 2222, '2010-05-05 09:30:00.000', GETDATE()),
    (33, 'English', 3333, '2010-07-20 14:45:00.000', GETDATE());


/*================================================================================ Score Table ================================================================================*/
CREATE TABLE Score (
    Sid INT,
    Cid INT,
    Score INT,
    Create_dt DATETIME
);

INSERT INTO Score (sid, cid, score, create_dt)
VALUES
    -- Scores for students born in 1990
    (6807, 11, 85, GETDATE()),
    (6807, 22, 86, GETDATE()),
    (6807, 33, 87, GETDATE()),
    (1315, 11, 82, GETDATE()),
    (1315, 22, 83, GETDATE()),
    (1315, 33, 84, GETDATE()),
    (1394, 11, 83, GETDATE()),
    (1394, 22, 84, GETDATE()),
    (1394, 33, 85, GETDATE()),
    (6588, 11, 80, GETDATE()),
    (6588, 22, 81, GETDATE()),
    (6588, 33, 82, GETDATE()),
    (8699, 11, 85, GETDATE()),
    (8699, 22, 86, GETDATE()),
    (8699, 33, 87, GETDATE()),
    (8326, 11, 87, GETDATE()),
    (8326, 22, 88, GETDATE()),
    (8326, 33, 89, GETDATE()),
    (3720, 11, 85, GETDATE()),
    (3720, 22, 86, GETDATE()),
    (3720, 33, 87, GETDATE()),
    (5360, 11, 90, GETDATE()),
    (5360, 22, 89, GETDATE()),
    (5360, 33, 88, GETDATE()),
    (8642, 11, 81, GETDATE()),
    (8642, 22, 82, GETDATE()),
    (8642, 33, 83, GETDATE()),
    (6348, 11, 75, GETDATE()),
    (6348, 22, 75, GETDATE()),
    (6348, 33, 75, GETDATE()),
    (3960, 11, 75, GETDATE()),
    (3960, 22, 75, GETDATE()),
    (3960, 33, 75, GETDATE()),
    (2933, 11, 75, GETDATE()),
    (2933, 22, 75, GETDATE()),
    (2933, 33, 75, GETDATE()),
    (2529, 11, 75, GETDATE()),
    (2529, 22, 75, GETDATE()),
    (2529, 33, 75, GETDATE()),
    (2899, 11, 75, GETDATE()),
    (2899, 22, 75, GETDATE()),
    (2899, 33, 75, GETDATE()),
    (3929, 11, 88, GETDATE()),
    (3929, 22, 89, GETDATE()),
    (3929, 33, 90, GETDATE()),
    -- Scores for students born in 1980
    (3038, 11, 55, GETDATE()),
    (3038, 22, 68, GETDATE()),
    (3038, 33, 68, GETDATE()),
    (7567, 11, 56, GETDATE()),
    (7567, 22, 68, GETDATE()),
    (7567, 33, 68, GETDATE()),
    (2498, 11, 57, GETDATE()),
    (2498, 22, 68, GETDATE()),
    (2498, 33, 68, GETDATE()),
    (2398, 11, 58, GETDATE()),
    (2398, 22, 68, GETDATE()),
    (2398, 33, 68, GETDATE()),
    (6576, 11, 59, GETDATE()),
    (6576, 22, 68, GETDATE()),
    (6576, 33, 68, GETDATE()),
    -- Scores for students born in 1995
    (5816, 11, 75, GETDATE()),
    (5816, 22, 76, GETDATE()),
    (5816, 33, 77, GETDATE()),
    (6473, 11, 78, GETDATE()),
    (6473, 22, 79, GETDATE()),
    (6473, 33, 80, GETDATE()),
    (3282, 11, 81, GETDATE()),
    (3282, 22, 82, GETDATE()),
    (3282, 33, 83, GETDATE()),
    (8574, 11, 84, GETDATE()),
    (8574, 22, 85, GETDATE()),
    (8574, 33, 86, GETDATE()),
    (2671, 11, 87, GETDATE()),
    (2671, 22, 88, GETDATE()),
    (2671, 33, 89, GETDATE()),
    (2345, 11, 90, GETDATE()),
    (2345, 22, 91, GETDATE()),
    (2345, 33, 92, GETDATE()),
    (9872, 11, 93, GETDATE()),
    (9872, 22, 94, GETDATE()),
    (9872, 33, 91, GETDATE()),
    (9462, 11, 92, GETDATE()),
    (9462, 22, 93, GETDATE()),
    (9462, 33, 94, GETDATE()),
    (5732, 11, 93, GETDATE()),
    (5732, 22, 94, GETDATE()),
    (5732, 33, 95, GETDATE());
