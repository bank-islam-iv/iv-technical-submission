/* Question 1: 
   How many student that having score between 75 to 80 in Mandarin but lower value than they having in other course */
SELECT COUNT(DISTINCT ST.Sid) AS 'TOTAL STUDENTS'
FROM STUDENT ST
JOIN SCORE SM ON ST.Sid = SM.Sid AND SM.Cid = 11 -- Mandarin course (assuming cid = 1)
JOIN SCORE SO ON ST.Sid = SO.Sid AND SO.Cid <> 11 -- Other courses (different than Mandarin)
WHERE SM.Score BETWEEN 75 AND 80
  AND SM.Score < SO.Score;




/* Question 2: 
   Create Segment of Course Score [100-85], [85-70], [70-60], [<60] and count number of students under those segments for all courses, column to display also are course and teacher's name */			   				 			  			  			 		   					
SELECT C.Name AS Course, T.Name AS Teacher,
	   COUNT(CASE WHEN SC.Score BETWEEN 85 AND 100 THEN 1 END) AS [100-85],
	   COUNT(CASE WHEN SC.Score BETWEEN 70 AND 84 THEN 1 END) AS [85-70],
	   COUNT(CASE WHEN SC.Score BETWEEN 60 AND 69 THEN 1 END) AS [70-60],
	   COUNT(CASE WHEN SC.Score < 60 THEN 1 END) AS [<60]
FROM SCORE SC
INNER JOIN STUDENT ST ON SC.Sid = ST.Sid
INNER JOIN COURSE C ON SC.Cid = C.cid
INNER JOIN TEACHER T ON C.Tid = T.Tid
GROUP BY C.Name, T.Name
ORDER BY Course, Teacher;




/* Question 3: 
   Display student name, gender and score where student has the same score but in different course. */	
SELECT DISTINCT ST.Name AS 'Student Name', ST.Gender, SC1.Score
FROM STUDENT ST
JOIN SCORE SC1 ON ST.Sid = SC1.Sid
JOIN SCORE SC2 ON ST.Sid = SC2.Sid AND SC1.Cid <> SC2.Cid
WHERE SC1.Score = SC2.Score
ORDER BY ST.Name;




/* Question 4: 
   Show top 3 highest in scoring from each course and display column course name, student name and student score. */	
WITH CteRank AS ( SELECT C.Name AS Course, 
						 ST.Name AS StudentName,
						 SC.Score AS StudentScore,
						 ROW_NUMBER() OVER(PARTITION BY C.Cid ORDER BY SC.Score DESC) AS Rank
				   FROM SCORE SC
				   INNER JOIN STUDENT ST ON SC.Sid = ST.Sid
				   INNER JOIN COURSE C ON SC.Cid = C.Cid
			     )

SELECT Course, StudentName, StudentScore
FROM CteRank
WHERE Rank <= 3
ORDER BY Course, StudentScore DESC;

